Feature: Register

    Scenario: Register Account
        Given I navigate to '/register'
        And I enter 'TestUsername' in field with id 'username'
        And I enter 'test@domain.tld' in field with id 'email'
        And I enter 'TestPassword' in field with id 'password'
        And I enter 'TestPassword' in field with id 'passwordVerify'
        And I click button with id 'submit'
        Then I get redirected to '/'

    Scenario: Register Account show error when not providing username
        Given I navigate to '/register'
        And I enter 'test@domain.tld' in field with id 'email'
        And I enter 'TestPassword' in field with id 'password'
        And I enter 'TestPassword' in field with id 'passwordVerify'
        And I click button with id 'submit'
        Then I see 'Please enter a username for your account'

    Scenario: Register Account show error when not providing email
        Given I navigate to '/register'
        And I enter 'TestUsername' in field with id 'username'
        And I enter 'TestPassword' in field with id 'password'
        And I enter 'TestPassword' in field with id 'passwordVerify'
        And I click button with id 'submit'
        Then I see 'Please specify a email for your account.'

    Scenario: Register Account show error when providing invalid email
        Given I navigate to '/register'
        And I enter 'TestUsername' in field with id 'username'
        And I enter 'domain.tld' in field with id 'email'
        And I enter 'TestPassword' in field with id 'password'
        And I enter 'TestPassword' in field with id 'passwordVerify'
        And I click button with id 'submit'
        Then I see 'Your email has no valid format.'

    Scenario: Register Account show error when not providing password
        Given I navigate to '/register'
        And I enter 'TestUsername' in field with id 'username'
        And I enter 'test@domain.tld' in field with id 'email'
        And I enter 'TestPassword' in field with id 'passwordVerify'
        And I click button with id 'submit'
        Then I see 'Please specify a password.'

    Scenario: Register Account show error when not providing passwordValidate
        Given I navigate to '/register'
        And I enter 'TestUsername' in field with id 'username'
        And I enter 'test@domain.tld' in field with id 'email'
        And I enter 'TestPassword' in field with id 'password'
        And I click button with id 'submit'
        Then I see 'Please validate your password.'

#    Scenario: Register Account show error when not having matching password
#        Given I navigate to '/register'
#        And I enter 'TestUsername' in field with id 'username'
#        And I enter 'test@domain.tld' in field with id 'email'
#        And I enter 'TestPassword' in field with id 'password'
#        And I enter 'TestPasswordMismatch' in field with id 'passwordVerify'
#        And I click button with id 'submit'
#        Then I see 'Your passwords do not match.'
