Feature: Add Credentials For Spotify

    Scenario: Spotify oauth server returns not a code
        Given I navigate to 'account/addcredentials/spotify'
        Then I see 'Uooops, there was a mistake'

    #Scenario: Spotify oauth server returns a code
    #    Given I navigate to 'account/addcredentials'
    #    When  I choose 'Spotify'
        #Then I get a button to 'https://accounts.spotify.com'
