import { expect } from 'chai';
import { Before, Given, setDefaultTimeout, Then, When } from 'cucumber';
import { AppPage } from '../../e2e/app.po';

setDefaultTimeout(60 * 1000);
let page: AppPage;

Before(() => {
    page = new AppPage();
});

Given(/^I navigate to '(.*)'$/, (route) => {
    return page.navigateTo(route);
});

When(/^I do nothing$/, () => {
});

When(/^I reload the page$/, () => {
    return page.reload();
});

When(/^I click button with id '(.*)'$/, (id) => {
    return page.clickElement(id);
});

When(/^I click element with id '(.*)'$/, (id) => {
    return page.clickElement(id);
});

When(/^I enter '(.*)' in field with id '(.*)'$/, (value, id) => {
    return page.fillField(id, value);
});

When(/^I enter current date in field with id '(.*)'$/, (id) => {
    return page.fillField(id, new Date().toLocaleDateString('en-US'));
});


Then(/^I get redirected to '(.*)'$/, async (route) => {
    expect(await page.getUrl()).to.equal(route);
});

Then(/^I should see the title$/, async () => {
    expect(await page.getTitle()).to.equal('PartyPlayer');
});

Then(/^I see '(.*)'$/, async (text) => {
    expect((await page.elementWithTextExists(text))).to.equal(true);
});

Then(/^Dialog '(.*)' is closed$/, str => {
    return !page.dialogIsVisible(str);
});

Then(/^Dialog '(.*)' is open$/, str => {
    return page.dialogIsVisible(str);
});
Given(/^I await$/, () => {
    return page.waitForAngular();
});
