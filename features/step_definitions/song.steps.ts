import { expect } from 'chai';
import { After, Before, Then, When } from 'cucumber';
import { by, ElementFinder } from 'protractor';
import { AppPage } from '../../e2e/app.po';

let page: AppPage;
let selectedSong: ElementFinder;

Before(() => {
    page = new AppPage();
});

After(() => {
    return page.resetStorage();
});

When(/^I select first song of playlist$/, () => {
    // page.getBrowser().sleep(5 * 1000);
    selectedSong = page.selectFirstSongOfPlaylist();
    return selectedSong;
});

When(/^I click element with class '(.*)' on selected song$/, (className) => {
    return selectedSong.element(by.css('.' + className)).click();
});

Then(/^Selected song should have ([0-9]+) votes$/, async (voteCount) => {
    const elementText = await selectedSong
        .element(by.css('.vote-count'))
        .getText();
    expect(elementText).to.equal(voteCount);
});
