Feature: Show event

    Scenario: Event data is displayed correctly
        Given I navigate to '/event/dummy-event-id'
        Then I see 'Dummy event name'
        Then I see 'Date: Dec 7, 2019'
        Then I see 'My dummy description.'
