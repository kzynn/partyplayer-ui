Feature: Vote song

    Scenario: Can vote song up
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-up-icon' on selected song
        Then Selected song should have 2 votes

    Scenario: Can vote song down
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-down-icon' on selected song
        Then Selected song should have 0 votes

    Scenario: Can only vote song once (up up)
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-up-icon' on selected song
        And I click element with class 'vote-song-up-icon' on selected song
        Then Selected song should have 2 votes

    Scenario: Can only vote song once (up down)
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-up-icon' on selected song
        And I click element with class 'vote-song-down-icon' on selected song
        Then Selected song should have 2 votes

    Scenario: Can only vote song once (down up)
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-down-icon' on selected song
        And I click element with class 'vote-song-up-icon' on selected song
        Then Selected song should have 0 votes

    Scenario: Can only vote song once (down down)
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-down-icon' on selected song
        And I click element with class 'vote-song-down-icon' on selected song
        Then Selected song should have 0 votes

    Scenario: Upvote is saved
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-up-icon' on selected song
        And I reload the page
        Then Selected song should have 2 votes

    Scenario: Downvote is saved
        Given I navigate to '/event/dummy-event-id'
        When I select first song of playlist
        And I click element with class 'vote-song-down-icon' on selected song
        And I reload the page
        Then Selected song should have 0 votes
