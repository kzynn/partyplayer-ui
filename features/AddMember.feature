Feature: Add member

    Scenario: Add member to event
        Given I navigate to '/event/dummy-event-id'
        And I click button with id 'addMember'
        And I enter 'user1' in field with id 'username'
        And I click button with id 'addMemberSubmit'
        Then Dialog 'addMemberDialog' is closed

    Scenario: Add member to event not given email
        Given I navigate to '/event/dummy-event-id'
        And I click button with id 'addMember'
        And I await
        And I click button with id 'addMemberSubmit'
        And Dialog 'addMemberDialog' is open

    Scenario: Add member to event cancel dialog
        Given I navigate to '/event/dummy-event-id'
        And I click button with id 'addMember'
        And I enter 'user1' in field with id 'username'
        And I click button with id 'addMemberCancel'
        Then Dialog 'addMemberDialog' is closed
