Feature: Create Event

    Scenario: Successfully create event with all data
        Given I navigate to '/event/create'
        When I enter 'My Event' in field with id 'eventName'
        And I enter current date in field with id 'eventDate'
        And I enter 'My custom event description' in field with id 'eventDescription'
        And I click button with id 'createEventBtn'
        Then I get redirected to '/event/any-event-id'

    Scenario: Show error message when not providing a name
        Given I navigate to '/event/create'
        And I enter current date in field with id 'eventDate'
        And I enter 'My custom event description' in field with id 'eventDescription'
        And I click button with id 'createEventBtn'
        Then I see 'Please give a name to your event.'

    Scenario: Show error message when not providing a date
        Given I navigate to '/event/create'
        When I enter 'My Event' in field with id 'eventName'
        And I enter 'My custom event description' in field with id 'eventDescription'
        And I click button with id 'createEventBtn'
        Then I see 'Please give a date to your event.'

    Scenario: Show error message when date is in past
        Given I navigate to '/event/create'
        When I enter 'My Event' in field with id 'eventName'
        And I enter '11/20/2019' in field with id 'eventDate'
        And I enter 'My custom event description' in field with id 'eventDescription'
        And I click button with id 'createEventBtn'
        Then I see 'The event date cannot be in the past.'
