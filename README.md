# Partyplayer UI

## Development environment
To ensure the communication between the Frontend and Backend, you need to provide a valid URL. If you are trying to run it via the `localhost` domain, there might occur some security errors, which restrict some features. This is especially the case with **Google Chrome**. In the worst case, the application gets unusable.

Make sure to add an entry into your `hosts` file.  
```127.0.0.1 partyplayer.l```

After that, execute 
``` bash
ng build partyplayer-api
ng serve --disable-host-check
```
to run the application. Make sure **not** to open the application with `localhost`, but with the domain specified in the `hosts` file. The Port remains the same.

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test-local` to execute the unit tests in a visible browser via [Karma](https://karma-runner.github.io).
Run `ng test` to execute the tests in a headless browser.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
