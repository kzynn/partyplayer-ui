import { browser, by, element, ElementFinder } from 'protractor';

export class AppPage {

    getBrowser() {
        return browser;
    }

    reload() {
        return browser.refresh();
    }

    resetStorage() {
        return browser.executeScript('window.localStorage.clear();');
    }

    clickElement(id: string): any {
        return browser.element(by.id(id)).click();
    }

    navigateTo(route: string) {
        return browser.get(route) as Promise<any>;
    }

    async getTitle(): Promise<string> {
        return browser.getTitle();
    }

    async getUrl(): Promise<string> {
        return browser.getCurrentUrl().then((url) => {
            return url.replace(browser.baseUrl, '/');
        });
    }

    async fillField(id, value) {
        return browser.element(by.id(id)).sendKeys(value);
    }

    async elementWithTextExists(text): Promise<boolean> {
        return element(by.xpath('//*[contains(., \'' + text + '\')]')).isDisplayed();
    }

    /*
        clickById(testId: string) {
            const formField = element(by.className('mat-select-arrow'));
            formField.click().then(() => {
                console.log('Clicked...........');
                element(by.cssContainingText('mat-option', 'Spotify')).click();
            }).catch((err) => {
                console.log(err);
            });
        }
     */

    async dialogIsVisible(id: string): Promise<boolean> {
        return browser.element(by.id(id)).isDisplayed();
    }

    selectFirstSongOfPlaylist(): ElementFinder {
        return element(by.css('mat-list > mat-list-item:first-of-type'));
    }

    waitForAngular() {
        browser.waitForAngular().then();
    }
}
