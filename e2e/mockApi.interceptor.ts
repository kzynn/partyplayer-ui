import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {EventResponse, SongResponse, UserCreationResponse} from 'partyplayer-api';
import {Observable, of} from 'rxjs';
import {environment} from './../src/environments/environment';

@Injectable()
export class MockApiInterceptor implements HttpInterceptor {
    // responses: {method: string, relUrl: string, responseBody: any}[] = [
    //    {method: 'POST', relUrl: '/events', responseBody: {}},
    // ];

    songs: SongResponse[] = [
        {
            id: 'song-id-1',
            title: 'Any Song title',
            artist: 'Any Artist',
            duration: 180,
            platform: 'spotify',
            playlink: 'any',
            votes: localStorage.getItem('song_vote_count') ? parseInt(localStorage.getItem('song_vote_count'), 10) : 1,
        },
    ];

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {
        if (
            req.method === 'POST' &&
            req.url === environment.apiUrl + '/events'
        ) {
            const body: EventResponse = req.body;
            return of(
                new HttpResponse<EventResponse>({
                    body: {
                        id: 'any-event-id',
                        name: body.name,
                        date: body.date,
                        description: body.description,
                    },
                }),
            );
        }

        if (
            req.method === 'GET' &&
            req.url === environment.apiUrl + '/events/dummy-event-id'
        ) {
            const body: EventResponse = req.body;
            return of(
                new HttpResponse<EventResponse>({
                    body: {
                        id: 'dummy-event-id',
                        name: 'Dummy event name',
                        date: new Date(2019, 11, 7),
                        description: 'My dummy description.',
                        invitationLink: null,
                        relation: 'owner',
                    },
                }),
            );
        }

        if (
            req.method === 'GET' &&
            req.url === environment.apiUrl + '/events/dummy-event-id/songs'
        ) {
            const body: EventResponse = req.body;
            return of(
                new HttpResponse<SongResponse[]>({
                    body: this.songs,
                }),
            );
        }

        if (
            req.method === 'POST' &&
            req.url === environment.apiUrl + '/account/register'
        ) {
            const body: UserCreationResponse = req.body;
            return of(
                new HttpResponse<UserCreationResponse>({
                    body: {
                        user: {
                            id: '1',
                            username: 'TestUsername',
                            email: 'test@domain.tld',
                        },
                        error: [],
                    },
                }),
            );
        }

        if (
            req.method === 'POST' &&
            req.url === environment.apiUrl + '/songs/song-id-1/upvotes'
        ) {
            localStorage.setItem('song_vote_count', '' + (this.songs[0].votes + 1));
            return of(new HttpResponse({status: 200}));
        }

        if (
            req.method === 'POST' &&
            req.url === environment.apiUrl + '/songs/song-id-1/downvotes'
        ) {
            localStorage.setItem('song_vote_count', '' + (this.songs[0].votes - 1));
            return of(new HttpResponse({status: 200}));
        }

        return of(new HttpResponse({status: 404}));
    }
}
