const puppeteer = require('puppeteer');
process.env.CHROME_BIN = puppeteer.executablePath();

exports.config = {
    allScriptsTimeout: 11000,
    specs: ['../features/**/*.feature'],
    capabilities: {
        chromeOptions: {
            args: ["--headless", "--no-sandbox"],
            binary: process.env.CHROME_BIN
        },
        'browserName': 'chrome'
    },
    directConnect: true,
    baseUrl: 'http://localhost:4200',
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    cucumberOpts: {
        require: ['../features/step_definitions/**/*.steps.ts'],
    },
    onPrepare() {
        require('ts-node').register({
            project: require('path').join(__dirname, './tsconfig.json')
        });
    }
};
