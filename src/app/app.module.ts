import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MockApiInterceptor} from 'e2e/mockApi.interceptor';
import {ApiModule, Configuration} from 'partyplayer-api';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AddSongComponent} from './components/event/add-song/add-song.component';
import {FoundSongComponent} from './components/event/add-song/found-song/found-song.component';
import {CreateEventPageComponent} from './components/event/create-event-page/create-event-page.component';
import {EventListComponent} from './components/event/event-list/event-list.component';
import {
    AddMemberDialog,
    DeleteEventDialog,
    EditEventDialog,
    EventPageComponent,
    RemoveMemberDialog,
} from './components/event/event-page/event-page.component';
import {InvitationComponent} from './components/event/invitation/invitation.component';
import {SongItemComponent} from './components/event/song-item/song-item.component';
import {LandingPageComponent} from './components/landing-page/landing-page.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {
    AccountComponent,
    DeleteTokenDialog,
    UpdateUsernameDialog
} from './components/profile/account/account.component';
import {AddCredentialsComponent} from './components/profile/account/add-credentials/add-credentials.component';
import {AddSpotifyCredentialsComponent} from './components/profile/account/add-credentials/add-spotify-credentials/add-spotify-credentials.component';
import {LoginComponent} from './components/profile/login/login.component';
import {RegisterComponent} from './components/profile/register/register.component';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {EventTmpUser} from './interceptor/eventTmpUser.interceptor';
import {SortSongsPipe} from './pipes/sort-song/sort-songs.pipe';
import {CurrentUserService} from './services/account/current-user.service';
import {ActivateComponent} from './components/profile/activate/activate.component';
import {PasswordresetComponent} from './components/profile/account/passwordreset/passwordreset.component';

@NgModule({
    declarations: [
        AppComponent,
        ToolbarComponent,
        LoginComponent,
        RegisterComponent,
        CreateEventPageComponent,
        EventPageComponent,
        SongItemComponent,
        SortSongsPipe,
        AccountComponent,
        AddCredentialsComponent,
        AddSpotifyCredentialsComponent,
        AddSongComponent,
        AddMemberDialog,
        LandingPageComponent,
        NotFoundComponent,
        FoundSongComponent,
        InvitationComponent,
        DeleteEventDialog,
        RemoveMemberDialog,
        EditEventDialog,
        DeleteTokenDialog,
        EventListComponent,
        ActivateComponent,
        UpdateUsernameDialog,
        PasswordresetComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatFormFieldModule,
        MatIconModule,
        MatToolbarModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatSnackBarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatListModule,
        MatTabsModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatDialogModule,
        HttpClientModule,
        ApiModule,
        MatTableModule,
    ],
    entryComponents: [
        AddMemberDialog,
        DeleteEventDialog,
        RemoveMemberDialog,
        EditEventDialog,
        DeleteTokenDialog,
        UpdateUsernameDialog,
    ],
    providers: [
        (environment.mock) ? {
            provide: HTTP_INTERCEPTORS,
            useClass: MockApiInterceptor,
            multi: true,
        } : [],
        CurrentUserService,
        {
            provide: Configuration,
            useFactory: (currentUserService: CurrentUserService) => new Configuration({
                basePath: environment.apiUrl,
                apiKeys: {Authorization: 'Bearer ' + currentUserService.getUserToken()},
            }),
            deps: [CurrentUserService],
            multi:
                false,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: EventTmpUser,
            multi: true,
        }],
    bootstrap: [AppComponent],
})

export class AppModule {
}
