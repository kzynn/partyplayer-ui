import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EventTmpUser implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.headers.get('Authorization') === 'Bearer EMPTY TOKEN'
            || request.headers.get('Authorization') === undefined
            || request.headers.get('Authorization') === null) {

            const url = request.url.split('events/')[1];
            if (url !== undefined) {
                const eventId = url.split('/')[0];

                console.log(eventId);

                const token = sessionStorage.getItem('TMP_JOIN_' + eventId);

                if (token !== undefined && token != null && token !== '') {
                    request = request.clone({
                        setHeaders: {
                            Authorization: `Bearer ${token}`,
                        },
                    });
                    return next.handle(request);
                }
            }
        }

        return next.handle(request);
    }
}
