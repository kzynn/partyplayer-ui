import { SongResponse } from 'partyplayer-api';
import { SortSongsPipe } from './sort-songs.pipe';

describe('SortSongsPipe', () => {
    let pipe: SortSongsPipe;

    beforeAll(() => {
        pipe = new SortSongsPipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('should sort songs by votes', () => {
        const songs: SongResponse[] = [
            {
                title: 'anySong',
                artist: 'johnny $',
                votes: 4,
            },
            {
                title: 'any other Song',
                artist: 'johnny $',
                votes: 0,
            },
            {
                title: 'just one more song',
                artist: 'johnny $',
                votes: 10,
            },
        ];

        expect(pipe.transform(songs, 'votes')).toEqual([
            {
                title: 'just one more song',
                artist: 'johnny $',
                votes: 10,
            },
            {
                title: 'anySong',
                artist: 'johnny $',
                votes: 4,
            },
            {
                title: 'any other Song',
                artist: 'johnny $',
                votes: 0,
            },
        ]);
    });

    it('should not switch songs with same votes count', () => {
        const songs: SongResponse[] = [
            {
                title: 'anySong',
                artist: 'johnny $',
                votes: 4,
            },
            {
                title: 'any other Song',
                artist: 'johnny $',
                votes: 4,
            },
        ];

        expect(pipe.transform(songs, 'votes')).toEqual([
            {
                title: 'anySong',
                artist: 'johnny $',
                votes: 4,
            },
            {
                title: 'any other Song',
                artist: 'johnny $',
                votes: 4,
            },
        ]);
    });

    it('should sort songs by date', () => {
        const songs: SongResponse[] = [
            {
                title: 'anySong',
                artist: 'johnny $',
                votes: 0,
                suggestedAt: new Date('2020-05-21T00:00:00'),
            },
            {
                title: 'any other Song',
                artist: 'johnny $',
                votes: 4,
                suggestedAt: new Date('2020-05-22T00:00:00'),
            },
            {
                title: 'just one more song',
                artist: 'johnny $',
                votes: 10,
                suggestedAt: new Date('2020-05-20T00:00:00'),
            },
        ];

        expect(pipe.transform(songs, 'date')).toEqual([
            {
                title: 'any other Song',
                artist: 'johnny $',
                votes: 4,
                suggestedAt: new Date('2020-05-22T00:00:00'),
            },
            {
                title: 'anySong',
                artist: 'johnny $',
                votes: 0,
                suggestedAt: new Date('2020-05-21T00:00:00'),
            },
            {
                title: 'just one more song',
                artist: 'johnny $',
                votes: 10,
                suggestedAt: new Date('2020-05-20T00:00:00'),
            },
        ]);
    });
});
