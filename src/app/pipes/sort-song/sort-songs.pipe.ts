import { Pipe, PipeTransform } from '@angular/core';
import { SongResponse, SongHistoryResponse } from 'partyplayer-api';

@Pipe({
    name: 'sortSongs',
    pure: false,
})
export class SortSongsPipe implements PipeTransform {
    transform(
        songs: SongResponse[]|SongHistoryResponse[],
        sortByProperty?: 'votes'|'date'|'history',
    ): SongResponse[]|SongHistoryResponse[] {
        if (sortByProperty === 'history') {
            const s: SongHistoryResponse[] = songs as SongHistoryResponse[];
            return s ? s.sort((s1, s2) => s2.playedAt < s1.playedAt ? -1 : 1) : s;
        }
        
        if (sortByProperty === 'votes') {
            return songs ? songs.sort((s1, s2) => s2.votes - s1.votes) : songs;
        }

        if (sortByProperty === 'date') {
            return songs ? songs.sort((s1, s2) => s2.suggestedAt > s1.suggestedAt ? 1 : -1) : songs;
        }

        return songs;
    }
}
