export interface Song {
    name: string;
    interpret: string;
    votes: number;
}
