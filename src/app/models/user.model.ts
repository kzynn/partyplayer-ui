export interface User {
    username?: string;
    email?: string;
    // Token does not contain "Bearer "
    token?: string;
}
