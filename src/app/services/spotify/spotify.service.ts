import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class SpotifyService {

    constructor() {

    }

    currentDeviceId: Subject<string> = new Subject<string>();
    songHasFinished: Subject<boolean> = new Subject<boolean>();

    private preSongs = 0;


    init(token: string) {
        // @ts-ignore
        window.onSpotifyWebPlaybackSDKReady = () => {
            // @ts-ignore
            const player = new Spotify.Player({
                name: 'Player',
                getOAuthToken: cb => {
                    cb(token);
                },
            });

            // Error handling
            player.addListener('initialization_error', (message) => {
                console.error(message);
            });
            player.addListener('authentication_error', ({message}) => {
                console.error(message);
            });
            player.addListener('account_error', ({message}) => {
                console.error(message);
            });
            player.addListener('playback_error', ({message}) => {
                console.error(message);
            });

            // Playback status updates
            player.addListener('player_state_changed', state => {
                this.preSongs = state.track_window.previous_tracks.length;
                if (this.preSongs === 0 && state.paused) {
                    this.songHasFinished.next(true);
                } else {
                    console.log('song is running');
                }
            });

            // Ready
            player.addListener('ready', ({device_id}) => {
                console.log('Ready with Device ID', device_id);
                this.currentDeviceId.next(device_id);
            });

            // Not Ready
            player.addListener('not_ready', ({device_id}) => {
                console.log('Device ID has gone offline', device_id);
            });

            // Connect to the player!
            player.connect();
        };
        // @ts-ignore
        window.onSpotifyWebPlaybackSDKReady();

    }
}
