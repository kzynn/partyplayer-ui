import {Injectable} from '@angular/core';
import {User} from '../../models/user.model';

const TOKEN = 'TOKEN';

@Injectable({
    providedIn: 'root',
})
export class CurrentUserService {

    private user: User;

    constructor() {
    }

    setUser(user: User): void {
        sessionStorage.setItem(TOKEN, user.token);
        this.user = user;
    }

    getUserToken(): string {
        if (sessionStorage.getItem(TOKEN) !== '' && sessionStorage.getItem(TOKEN) != null) {
            return sessionStorage.getItem(TOKEN);
        } else {
            return 'EMPTY TOKEN';
        }
    }

    isLoggedIn() {
        return sessionStorage.getItem(TOKEN) !== '' && sessionStorage.getItem(TOKEN) != null;
    }

    logout() {
        sessionStorage.removeItem(TOKEN);
    }
}
