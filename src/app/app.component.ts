import {Component, OnInit} from '@angular/core';
import {SpotifyService} from './services/spotify/spotify.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    title = 'partyplayer-ui';

    constructor() {
    }

    ngOnInit(): void {
    }

}
