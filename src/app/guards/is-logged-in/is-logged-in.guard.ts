import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { CurrentUserService } from '../../services/account/current-user.service';

@Injectable({
  providedIn: 'root',
})
export class IsLoggedInGuard implements CanActivate {

    constructor(
        private router: Router,
        private currentUserService: CurrentUserService,
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!this.currentUserService.isLoggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

}
