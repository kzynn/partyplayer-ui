import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {EventResponse, EventService} from 'partyplayer-api';

@Component({
    selector: 'app-event-list',
    templateUrl: './event-list.component.html',
    styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent implements OnInit {
    public events$: Observable<EventResponse[]>;
    displayedColumns: string[] = ['name', 'description', 'date'];

    constructor(private eventService: EventService) {
        this.events$ = eventService.getAllEventsForUser();
    }

    ngOnInit() {
    }

}
