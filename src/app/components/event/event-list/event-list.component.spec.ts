import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventListComponent} from './event-list.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule, MatTableModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {Configuration, EventService} from 'partyplayer-api';
import {CurrentUserService} from '../../../services/account/current-user.service';
import {environment} from '../../../../environments/environment';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('EventListComponent', () => {
    let component: EventListComponent;
    let fixture: ComponentFixture<EventListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EventListComponent],
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
                MatTableModule,
            ],
            providers: [
                {
                    provide: Configuration,
                    useFactory: (currentUserService: CurrentUserService) => new Configuration({
                        basePath: environment.apiUrl,
                        apiKeys: {Authorization: 'Bearer ' + currentUserService.getUserToken()},
                    }),
                    deps: [CurrentUserService],
                    multi:
                        false,
                },
                EventService,
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EventListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
