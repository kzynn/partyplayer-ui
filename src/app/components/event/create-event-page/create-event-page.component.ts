import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {EventService} from 'partyplayer-api';

export function noPastDateValidator(): ValidatorFn {
    return (control: FormControl): { [key: string]: any } | null => {
        const inputDate = Date.parse(control.value);
        const minDate = new Date().setHours(0, 0, 0, 0);

        if (control.value == null) {
            return null;
        }

        return inputDate < minDate
            ? {noPastDate: {value: control.value}}
            : null;
    };
}

@Component({
    selector: 'app-create-event-page',
    templateUrl: './create-event-page.component.html',
    styleUrls: ['./create-event-page.component.scss'],
})
export class CreateEventPageComponent implements OnInit {
    event = new FormGroup({
        name: new FormControl('', [Validators.required]),
        description: new FormControl('', []),
        date: new FormControl('', [Validators.required, noPastDateValidator()]),
    });

    minEventDate = new Date();

    constructor(private router: Router, private eventService: EventService) {
    }

    ngOnInit() {
    }

    create(): void {
        if (this.event.valid) {
            console.log('name: ' + this.getName().value);
            console.log('descr: ' + this.getDescription().value);
            console.log('date: ' + this.getDate().value);

            this.eventService
                .eventCreate({
                    name: this.getName().value,
                    description: this.getDescription().value,
                    date: this.getDate().value,
                }, 'body')
                .subscribe(event => {
                    this.router.navigateByUrl('/event/' + event.id);
                }, error => {
                    console.log('Error: could not create event.');
                    console.log(error);
                });
        }
    }

    getNameErrorMessage(): string {
        return this.getName().hasError('required')
            ? 'Please give a name to your event.'
            : 'Some error occured.';
    }

    getDateErrorMessage(): string {
        if (this.getDate().hasError('required')) {
            return 'Please give a date to your event.';
        }

        if (this.getDate().hasError('noPastDate')) {
            return 'The event date cannot be in the past.';
        }
    }

    private getName() {
        return this.event.get('name');
    }

    private getDate() {
        return this.event.get('date');
    }

    private getDescription() {
        return this.event.get('description');
    }
}
