import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {Configuration, EventService} from 'partyplayer-api';
import {environment} from '../../../../environments/environment';
import {CurrentUserService} from '../../../services/account/current-user.service';
import {ApiModule} from './../../../../../projects/partyplayer-api/src/api.module';
import {CreateEventPageComponent} from './create-event-page.component';


describe('CreateEventPageComponent', () => {
    let component: CreateEventPageComponent;
    let fixture: ComponentFixture<CreateEventPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                MatDatepickerModule,
                MatNativeDateModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule,
                ApiModule,
            ],
            providers: [
                EventService,
                {
                    provide: Configuration,
                    useFactory: (currentUserService: CurrentUserService) => new Configuration({
                        basePath: environment.apiUrl,
                        apiKeys: {Authorization: 'Bearer ' + currentUserService.getUserToken()},
                    }),
                    deps: [CurrentUserService],
                    multi:
                        false,
                },
            ],
            declarations: [CreateEventPageComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateEventPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
