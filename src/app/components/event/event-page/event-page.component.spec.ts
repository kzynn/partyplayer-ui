import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {MatDialog, MatSnackBarModule} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {Configuration, EventService, SpotifyService as SpotifyApi} from 'partyplayer-api';
import {of, Subject} from 'rxjs';
import {SortSongsPipe} from 'src/app/pipes/sort-song/sort-songs.pipe';
import {environment} from '../../../../environments/environment';
import {CurrentUserService} from '../../../services/account/current-user.service';
import {AddMemberDialog, EventPageComponent} from './event-page.component';
import {SpotifyService} from '../../../services/spotify/spotify.service';
import {Mock} from 'protractor/built/driverProviders';

class SpotifyMoc extends SpotifyService{
    currentDeviceId: Subject<string> = new Subject<string>();
    songHasFinished: Subject<boolean> = new Subject<boolean>();

    init(token: string) {
        console.log('hi');
    }
}


describe('EventPageComponent', () => {
    let spotifyMoc: SpotifyMoc;
    let component: EventPageComponent;
    let fixture: ComponentFixture<EventPageComponent>;

    beforeEach(async(() => {
        spotifyMoc = new SpotifyMoc();
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                MatDialogModule,
                MatSnackBarModule,
                RouterTestingModule,
                FormsModule,
            ],
            providers: [
                {
                    provide: Configuration,
                    useFactory: (currentUserService: CurrentUserService) => new Configuration({
                        basePath: environment.apiUrl,
                        apiKeys: {Authorization: 'Bearer ' + currentUserService.getUserToken()},
                    }),
                    deps: [CurrentUserService],
                    multi:
                        false,
                },
                {
                    provide: SpotifyService,
                    useValue: spotifyMoc,
                },
                EventService,
                SpotifyApi,
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({eventId: 'asfasf'}),
                        snapshot: {},
                    },
                },
            ],
            declarations: [EventPageComponent, SortSongsPipe, AddMemberDialog],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EventPageComponent);
        component = fixture.componentInstance;
        component.dialog = TestBed.get(MatDialog);
        fixture.detectChanges();
    });

    it('should create', () => {

        expect(component).toBeTruthy();
    });
});
