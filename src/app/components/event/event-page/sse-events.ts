export interface VoteEvent {
    id: string;
    votes: number;
}

export interface PlayEvent {
    id: string;
}
