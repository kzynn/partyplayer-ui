import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {
    CreateEventDTO,
    EventResponse,
    EventService,
    SongHistoryResponse,
    SongResponse,
    SpotifyService as SpotifyApi,
    UserResponse,
} from 'partyplayer-api';
import {Observable, of, ReplaySubject, Subject} from 'rxjs';
import {catchError, concatAll, map} from 'rxjs/operators';
import {noPastDateValidator} from '../create-event-page/create-event-page.component';
import {CurrentUserService} from './../../../services/account/current-user.service';
import {EventSourceWrapper} from './eventSourceWrapper';
import {environment} from '../../../../environments/environment';
import {SpotifyService} from '../../../services/spotify/spotify.service';
import {PlayEvent, VoteEvent} from './sse-events';


@Component({
    selector: 'app-event-page',
    templateUrl: './event-page.component.html',
    styleUrls: ['./event-page.component.scss'],
})
export class EventPageComponent implements OnInit {
    event$ = new ReplaySubject<EventResponse>(1);
    eventLoadingError$ = new Subject<boolean>();
    songs: SongResponse[];
    songsHistory: SongHistoryResponse[];

    currentSong: SongResponse = null;
    eventId: string;


    constructor(
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private eventService: EventService,
        private snackBar: MatSnackBar,
        private spotifyService: SpotifyService,
        private spotifyApi: SpotifyApi,
        private currentUserService: CurrentUserService,
    ) {
        this.refreshEvent();
        this.getSongs();
        this.getPlayedSongs();
        this.initSSE();
    }

    refreshEvent(): void {
        this.route.params
            .pipe(
                map((params) => params.eventId),
                map((id) => this.eventService.getEventById(id)),
                concatAll(),
                catchError((error) => {
                    console.error('error loading the event', error);
                    this.eventLoadingError$.next(true);
                    return of();
                }),
            )
            .subscribe(this.event$);
    }

    private getSongs(): void {
        this.route.params
            .pipe(
                map((params) => params.eventId),
                map((id) => this.eventService.eventGetSongs(id)),
                concatAll(),
                catchError((error) => {
                    console.log(error);
                    return of([] as SongResponse[]);
                }),
            )
            .subscribe(songs => {
                this.songs = songs;
            });
    }

    private getPlayedSongs(): void {
        this.route.params
            .pipe(
                map((params) => params.eventId),
                map((id) => this.eventService.getPlayedSongs(id)),
                concatAll(),
                catchError((error) => {
                    console.log(error);
                    return of([] as SongHistoryResponse[]);
                }),
            )
            .subscribe(songs => {
                this.songsHistory = songs;
            });
    }

    trackSong(index: number, song: SongResponse | SongHistoryResponse): string {
        return song.id;
    }

    openEditDialog(): void {
        this.event$.subscribe(event => {
            const dialogRef = this.dialog.open(EditEventDialog, {
                id: 'editEventDialog',
                height: 'auto',
                width: 'auto',
                data: event,
            });

            dialogRef.afterClosed().subscribe(editedEvent => {
                if (editedEvent && editedEvent.valid) {
                    const ev: CreateEventDTO = {
                        name: editedEvent.value.name,
                        description: editedEvent.value.description,
                        date: editedEvent.value.date,
                    };

                    this.eventService.updateEventById(event.id, ev, 'body')
                        .subscribe(t => this.refreshEvent());
                }
            });
        });
    }

    openAddMemberDialog(): void {
        this.event$.subscribe((e) => {
            this.dialog.open(AddMemberDialog, {
                id: 'addMemberDialog',
                height: 'auto',
                width: 'auto',
                data: e.id,
            });
        });
    }

    openDeleteEventDialog(): void {
        this.event$.subscribe((e) => {
            this.dialog.open(DeleteEventDialog, {
                id: 'deleteEventDialog',
                height: 'auto',
                width: 'auto',
                data: e.id,
            });
        });
    }

    openRemoveMemberDialog(): void {
        this.event$.subscribe(e => {
            this.dialog.open(RemoveMemberDialog, {
                id: 'removeMemberDialog',
                height: 'auto',
                width: 'auto',
                data: e.id,
            });
        });
    }

    ngOnInit() {
    }

    private initSSE(): void {
        this.event$.subscribe(event => {
            this.eventId = event.id;
            const url = `${environment.apiUrl}/events/${event.id}/song-flux`;
            const options = {
                headers: {Authorization: `Bearer ${this.currentUserService.getUserToken()}`},
            };

            const sse = new EventSourceWrapper(url, options);

            sse.addEventListener('vote-event', (e: any) => {
                const ev: VoteEvent = JSON.parse(e.data);
                const song = this.songs[this.songs.findIndex(s => s.id === ev.id)];
                if (song) {
                    song.votes = ev.votes;
                }
            }, false);

            sse.addEventListener('add-song-event', (e: any) => {
                const ev: SongResponse = JSON.parse(e.data);
                this.songs.push(ev);
            }, false);

            sse.addEventListener('play-song-event', (e: any) => {
                const ev: PlayEvent = JSON.parse(e.data);
                const index = this.songs.findIndex(s => s.id === ev.id);

                if (index !== -1) {
                    const song = this.songs[index];
                    this.songs.splice(this.songs.indexOf(song), 1);
                    this.songsHistory.push(song);
                }
            }, false);

            sse.addEventListener('open', (e) => {
                console.log('SSE connected');
            }, false);

            sse.addEventListener('error', (e: any) => {
                if (e.readyState === EventSource.CLOSED) {
                    // Connection was closed.
                    console.log('SSE closed');
                }
            }, false);
        });
    }


    play() {
        this.currentSong = this.songs[0];

        this.spotifyApi.getSpotifyTokenToPlay().subscribe(value => {
            this.spotifyService.init(value.spotifyWebToken);
            // tslint:disable-next-line:no-shadowed-variable
            this.spotifyService.currentDeviceId.subscribe(value => {
                this.playFistSong(this.currentSong.playlink, value);
            });

            this.eventService.moveSongToHistory(
                this.eventId,
                this.currentSong.id,
            ).subscribe(data => {});
        });

    }

    playFistSong(trackId, deviceId: string) {
        const payload = {device: deviceId, playLink: trackId};
        this.spotifyApi.createSpotifyTokenToPlay(payload)
            .subscribe(val => console.log(val));
    }


    async retrieveInvitationLink() {
        let link = '';

        await this.event$.subscribe((sEvent) => {
            link = sEvent.invitationLink;

            console.log(link);

            if (link === undefined || link === null || link === '') {
                this.event$.subscribe((sEvent2) => {
                    this.eventService
                        .eventJoinLink(sEvent2.id)
                        .subscribe((res) => {
                            console.log(res);

                            link = res.link;
                            this._copyToClipboard(link);

                            this.snackBar.open('Invitation Link copied', null, {
                                duration: 2000,
                            });
                        });
                });
            } else {
                console.log(link);

                this._copyToClipboard(link);

                this.snackBar.open('Invitation Link copied', null, {
                    duration: 2000,
                });
            }
        });
    }

    private _copyToClipboard(val: string) {
        const selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = `https://partyplayer.xyz/event/${this.eventId}/invitation/${val}`;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
    }

    public leaveEventAsTmpUser() {
        this.event$.subscribe(e => {
            sessionStorage.removeItem('TMP_JOIN_' + e.id);
        });
    }

    public isTmpUser() {
        this.event$.subscribe(e => {
            return sessionStorage.getItem('TMP_JOIN_' + e.id) !== null;
        });
    }
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'event-delete-event-dialog',
    templateUrl: 'event-delete-event-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class DeleteEventDialog implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<DeleteEventDialog>,
        private router: Router,
        private eventService: EventService,
        @Inject(MAT_DIALOG_DATA) private data: string,
    ) {
    }

    ngOnInit(): void {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    deleteEvent(): void {
        this.eventService.deleteEventById(this.data).subscribe();
        this.dialogRef.close();
        this.router.navigateByUrl('/');
    }
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'event-add-member-dialog',
    templateUrl: 'event-add-member-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class AddMemberDialog implements OnInit {
    public username: string;

    constructor(
        public dialogRef: MatDialogRef<AddMemberDialog>,
        private eventService: EventService,
        @Inject(MAT_DIALOG_DATA) private data: string,
    ) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    addMember(): void {
        console.log(this.username);
        if (this.username != null) {
            console.log('in if');
            this.eventService
                .eventInviteUser(this.data, {username: this.username})
                .subscribe();
            this.dialogRef.close();
        }
    }

    ngOnInit(): void {
    }
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'event-edit-event-dialog',
    templateUrl: 'event-edit-event-dialog.html',
    styleUrls: ['./event-edit-event-dialog.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class EditEventDialog implements OnInit {

    public event;
    public minEventDate = new Date();

    constructor(public dialogRef: MatDialogRef<EditEventDialog>,
                @Inject(MAT_DIALOG_DATA) public data: EventResponse,
    ) {
        this.event = new FormGroup({
            name: new FormControl(this.data.name, [Validators.required]),
            description: new FormControl(this.data.description, []),
            date: new FormControl(this.data.date, [Validators.required, noPastDateValidator()]),
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit(): void {
    }

    getNameErrorMessage(): string {
        return this.getName().hasError('required')
            ? 'Please give a name to your event.'
            : 'Some error occured.';
    }

    getDateErrorMessage(): string {
        if (this.getDate().hasError('required')) {
            return 'Please give a date to your event.';
        }

        if (this.getDate().hasError('noPastDate')) {
            return 'The event date cannot be in the past.';
        }
    }

    private getName() {
        return this.event.get('name');
    }

    private getDate() {
        return this.event.get('date');
    }
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'event-remove-member-dialog',
    templateUrl: 'event-remove-member-dialog.html',
    styles: ['#deleteMember {cursor: pointer;}'],
})
// tslint:disable-next-line:component-class-suffix
export class RemoveMemberDialog implements OnInit {
    public members: Observable<UserResponse[]>;

    constructor(public dialogRef: MatDialogRef<AddMemberDialog>,
                public eventService: EventService,
                @Inject(MAT_DIALOG_DATA) private data: string,
    ) {
        this.members = eventService.eventGetMembers(data);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    deleteMember(memberId: string): void {
        this.eventService.eventRemoveMember(this.data, memberId).subscribe(u =>
            this.members = this.eventService.eventGetMembers(this.data),
        );
    }

    ngOnInit(): void {
    }
}
