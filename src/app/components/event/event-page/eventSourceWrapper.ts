import { EventSourcePolyfill } from 'event-source-polyfill';
import { environment } from '../../../../environments/environment';

export class EventSourceWrapper implements EventSourcePolyfill {
    eventSource: EventSourcePolyfill;

    constructor(url: string, options: any) {
        if (!environment.mock) {
            this.eventSource = new EventSourcePolyfill(url, options);
        }
    }

    addEventListener(event: string, callback: any, bool: boolean) {
        if (!environment.mock) {
            this.eventSource.addEventListener(event, callback, bool);
        }
    }
}
