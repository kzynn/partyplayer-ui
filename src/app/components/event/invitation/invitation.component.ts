import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CurrentUserService} from '../../../services/account/current-user.service';
import {EventService} from 'projects/partyplayer-api/src';

@Component({
    selector: 'app-event-page',
    templateUrl: './invitation.component.html',
    styleUrls: ['./invitation.component.scss'],
})
export class InvitationComponent implements OnInit {
    public decide = true;
    public error = '';
    private id = '';
    private link = '';
    public url = this.router.routerState.snapshot.url;

    constructor(private route: ActivatedRoute, private currentUserService: CurrentUserService,
                private eventService: EventService, private router: Router) {
        this.route.paramMap.subscribe(params => {
            this.id = params.get('eventId');
            this.link = params.get('link');
        });

        if (this.currentUserService.isLoggedIn()) {
            this.decide = false;
            this.eventService.acceptInvitation(this.id, this.link)
                .subscribe((res) => {
                        this.router.navigateByUrl('/event/' + this.id);
                    },
                    (err) => {
                        console.log(err);
                    });
        }
    }

    ngOnInit() {
    }

    async asGuest() {
        this.eventService.eventGuestJWT(this.id, {invitationLink: this.link})
            .subscribe((res) => {
                    sessionStorage.setItem('TMP_JOIN_' + this.id, res.token);
                    this.router.navigateByUrl('/event/' + this.id);
                },
                () => {
                    this.error = 'Das hat wohl nicht geklappt.';
                });
    }
}

