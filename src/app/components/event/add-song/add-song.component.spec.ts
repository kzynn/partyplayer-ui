import {HttpClientModule} from '@angular/common/http';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule, MatNativeDateModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {EventService, SongService} from 'partyplayer-api';
import {AddSongComponent} from './add-song.component';

describe('CreateEventPageComponent', () => {
    let component: AddSongComponent;
    let fixture: ComponentFixture<AddSongComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatNativeDateModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule,
                HttpClientModule,
                MatAutocompleteModule,
            ],
            declarations: [AddSongComponent],
            providers: [SongService, EventService],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddSongComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
