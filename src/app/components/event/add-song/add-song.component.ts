import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EventResponse, EventService, SearchResult, SongService } from 'partyplayer-api';
import { of, ReplaySubject, Subject } from 'rxjs';
import { catchError, concatAll, map } from 'rxjs/operators';

@Component({
    selector: 'app-add-song',
    templateUrl: './add-song.component.html',
    styleUrls: ['./add-song.component.scss'],
})
export class AddSongComponent implements OnInit {

    event$ = new ReplaySubject<EventResponse>(1);
    eventLoadingError$ = new Subject<boolean>();

    song: SearchResult;
    songs: SearchResult[];

    title: string;
    private eventId: string;
    myControl = new FormControl();

    constructor(
        private songService: SongService,
        private eventService: EventService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.title = '';

        this.route.params
            .pipe(
                map((params) => params.eventId),
                map((id) => this.eventService.getEventById(id)),
                concatAll(),
                catchError((error) => {
                    console.error('error loading the event', error);
                    this.eventLoadingError$.next(true);
                    return of();
                }),
            )
            .subscribe((event: EventResponse) => {
                this.event$.next(event);
                this.eventId = event.id;
            });
    }

    searchSong() {
        this.songService.searchSong(this.title).subscribe(s => {
            this.songs = s;
        });

    }

    selectSong(s: SearchResult) {
        this.song = s;
    }

    addSong() {
        this.eventService
            .eventAddSong(this.eventId, {
                title: this.song.name,
                artist: this.song.author,
                platform: this.song.platform,
                playlink: this.song.uniqueAccess,
                duration: 0,
            })
            .subscribe(s => {
                this.router.navigate([`event/${this.eventId}`]);
            });
    }
}
