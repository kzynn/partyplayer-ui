import {Component, Input, OnInit} from '@angular/core';
import {SearchResult} from 'partyplayer-api';

@Component({
    selector: 'app-found-song',
    templateUrl: './found-song.component.html',
    styleUrls: ['./found-song.component.scss'],
})
export class FoundSongComponent implements OnInit {

    @Input() isSelected: boolean;
    @Input() song: SearchResult;

    constructor() {
    }

    ngOnInit() {
    }

}
