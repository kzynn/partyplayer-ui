import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FoundSongComponent} from './found-song.component';
import {MatAutocompleteModule, MatNativeDateModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('FoundSongComponent', () => {
    let component: FoundSongComponent;
    let fixture: ComponentFixture<FoundSongComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatNativeDateModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule,
                MatAutocompleteModule,
            ],
            declarations: [FoundSongComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FoundSongComponent);
        component = fixture.componentInstance;
        component.song = {name: 'any Name', author: 'any author', uniqueAccess: 'any ua', platform: 'any platforms'};
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
