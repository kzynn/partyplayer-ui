import { environment } from './../../../../environments/environment';
import {Component, Input, OnInit} from '@angular/core';
import {SongResponse, SongService} from 'partyplayer-api';

@Component({
    selector: 'app-song-item',
    templateUrl: './song-item.component.html',
    styleUrls: ['./song-item.component.scss'],
})
export class SongItemComponent implements OnInit {
    @Input() song: SongResponse;
    @Input() votable = true;

    voted: boolean;

    constructor(
        private readonly songsApi: SongService,
    ) {}

    ngOnInit() {
        this.voted = localStorage.getItem(this.song.id) != null;
    }

    vote(upVote: boolean): void {
        if (this.votable && !this.voted) {
            if (upVote) {
                this.songsApi.voteSongUp(this.song.id).subscribe(() => {
                    if (environment.mock) {this.song.votes += 1; }
                    this.voted = true;
                    localStorage.setItem(this.song.id, '');
                }, error => {
                    console.log('Error occured while voting song.');
                });
            } else {
                this.songsApi.voteSongDown(this.song.id).subscribe(() => {
                    if (environment.mock) {this.song.votes -= 1; }
                    this.voted = true;
                    localStorage.setItem(this.song.id, '');
                }, error => {
                    console.log('Error occured while voting song.');
                });
            }
        }
    }
}
