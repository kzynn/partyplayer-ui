import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Configuration, SongService} from 'partyplayer-api';
import {CurrentUserService} from 'src/app/services/account/current-user.service';
import {environment} from 'src/environments/environment';
import {SongItemComponent} from './song-item.component';


describe('SongItemComponent', () => {
    let component: SongItemComponent;
    let fixture: ComponentFixture<SongItemComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                SongService,
                {
                    provide: Configuration,
                    useFactory: (currentUserService: CurrentUserService) => new Configuration({
                        basePath: environment.apiUrl,
                        apiKeys: {Authorization: 'Bearer ' + currentUserService.getUserToken()},
                    }),
                    deps: [CurrentUserService],
                    multi: false,
                },
            ],
            declarations: [SongItemComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SongItemComponent);
        component = fixture.componentInstance;
        component.votable = true;
        component.voted = false;
        component.song = { id: 'any-song-id', title: 'SongName', artist: 'Udo', votes: 0 };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
