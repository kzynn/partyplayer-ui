import {Component, OnInit} from '@angular/core';
import {CurrentUserService} from '../../services/account/current-user.service';

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {

    public loggedIn: boolean;

    constructor(userService: CurrentUserService) {
        this.loggedIn = userService.isLoggedIn();
    }

    ngOnInit() {
    }

}
