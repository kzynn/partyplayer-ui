import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CurrentUserService} from '../../services/account/current-user.service';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
    loggedIn: boolean;
    logo = '';
    action = '';
    link = '';

    constructor(private currentUserService: CurrentUserService, private router: Router) {
        this.changeToolBarField();
    }

    ngOnInit() {
    }

    changeToolBarField(): void {
        this.router.events.subscribe((val) => {
            this.loggedIn = this.currentUserService.isLoggedIn();
            if (this.loggedIn) {
                this.logo = 'account_circle';
                this.action = 'Account';
                this.link = '/account';
            } else {
                this.logo = 'vpn_key';
                this.action = 'Login';
                this.link = '/login';
            }
        });
    }
}
