import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AccountService, UserCreationResponse} from 'partyplayer-api';
import {Observable} from 'rxjs';

export function passwordVerifyCheck(): ValidatorFn {
    return (control: FormGroup): ValidationErrors => {
        const pass = control.controls.password.value;
        const passVerify = control.controls.passwordVerify.value;

        return (passVerify !== pass)
            ? {mismatch: true}
            : null;
    };
}

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

    hide = true;

    event = new FormGroup({
        username: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.email, Validators.required]),
        passwords: new FormGroup({
            password: new FormControl('', [Validators.required]),
            passwordVerify: new FormControl('', [Validators.required]),
        }, passwordVerifyCheck()),
    });

    constructor(private router: Router, private accountService: AccountService) {
    }

    ngOnInit() {
    }

    create(): void {
        if (this.event.valid) {

            const userData = {
                email: this.getEmail().value,
                password: this.getPassword().value,
                username: this.getUsername().value,
            };

            let response: Observable<UserCreationResponse>;
            response = this.accountService.accountRegister(userData);


            response.subscribe(res => {
                    const user = res.user;
                    const error = res.error;

                    if (error.length === 0) {
                        this.router.navigateByUrl('');
                    }
                }, error => {
                    error.error.error.forEach(val => {
                        const fnName: string = 'get' + val.charAt(0).toUpperCase() + val.substring(1);
                        this[fnName]().setErrors({inUse: true});
                    });
                },
            );
        }
    }

    getUsernameErrorMessage(): string {
        if (this.getUsername().hasError('required')) {
            return 'Please enter a username for your account';
        }

        if (this.getUsername().hasError('inUse')) {
            return 'This username is already taken';
        }
    }

    getEmailErrorMessage(): string {
        if (this.getEmail().hasError('required')) {
            return 'Please specify a email for your account.';
        }

        if (this.getEmail().hasError('email')) {
            return 'Your email has no valid format.';
        }

        if (this.getEmail().hasError('inUse')) {
            return 'This email is already taken';
        }
    }

    getPasswordErrorMessage(): string {
        if (this.getPassword().hasError('required')) {
            return 'Please specify a password.';
        }
    }

    getPasswordVerifyErrorMessage(): string {
        if (this.getPasswordVerify().hasError('required')) {
            return 'Please validate your password.';
        }

        if (this.getPasswordGroup().hasError('mismatch')) {
            return 'Your passwords do not match.';
        }
    }

    private getUsername() {
        return this.event.get('username');
    }

    private getEmail() {
        return this.event.get('email');
    }

    private getPasswordGroup() {
        return this.event.get('passwords');
    }

    private getPassword() {
        return this.event.get('passwords').get('password');
    }

    private getPasswordVerify() {
        return this.event.get('passwords').get('passwordVerify');
    }
}
