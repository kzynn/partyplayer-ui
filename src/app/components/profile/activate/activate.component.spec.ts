import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActivateComponent} from './activate.component';
import {AccountService} from 'partyplayer-api';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ActivateComponent', () => {
    let component: ActivateComponent;
    let fixture: ComponentFixture<ActivateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule],
            declarations: [ActivateComponent],
            providers: [AccountService],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ActivateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
