import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AccountService} from 'partyplayer-api';

@Component({
    selector: 'app-activate',
    templateUrl: './activate.component.html',
    styleUrls: ['./activate.component.scss'],
})
export class ActivateComponent implements OnInit {

    public message = 'Deine Anfrage wird verarbeitet';
    public showLogin = false;
    private email = '';
    private token = '';

    constructor(private route: ActivatedRoute, private accountService: AccountService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.email = params.email;
            this.token = params.token;
        });

        this.validate();
    }

    public validate() {
        if (this.email === undefined || this.token === undefined) {
            this.message = 'Sorry, diene Anfrage konnte nicht verarbeitet werden. Ist der Link korrekt?';
            return;
        }

        this.accountService
            .accountActivate(this.email, this.token)
            .subscribe(() => {
                    this.message = 'Dein Account wurde aktiviert, du kannst dich jetzt eionloggen!';
                    this.showLogin = true;
                },
                () => {
                    this.message = 'Dein Account konnte nicht aktiviert werden. Ist der Link korrekt?';
                    this.showLogin = false;
                });
    }
}
