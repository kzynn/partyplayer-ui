import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SpotifyService} from 'partyplayer-api';

@Component({
    selector: 'app-add-spotify-credentials',
    templateUrl: './add-spotify-credentials.component.html',
    styleUrls: ['./add-spotify-credentials.component.scss']
})
export class AddSpotifyCredentialsComponent implements OnInit {


    gotCode: boolean;

    constructor(private route: ActivatedRoute, private spotify: SpotifyService, private router: Router) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            const code = params.code;
            if (code === undefined) {
                this.gotCode = false;
                return;
            }
            this.gotCode = true;
            this.spotify.createAccessToken({code}).subscribe(s => {
                this.router.navigate(['/account']);
            });
        });
    }
}
