import {HttpClientModule} from '@angular/common/http';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {EventService, SpotifyService} from 'partyplayer-api';
import {AddSpotifyCredentialsComponent} from './add-spotify-credentials.component';


describe('AddSpotifyCredentialsComponent', () => {
    let component: AddSpotifyCredentialsComponent;
    let fixture: ComponentFixture<AddSpotifyCredentialsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddSpotifyCredentialsComponent],
            imports: [
                RouterTestingModule,
                HttpClientModule,
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [SpotifyService, EventService],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddSpotifyCredentialsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
