import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddCredentialsComponent} from './add-credentials.component';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EventService} from 'partyplayer-api';

describe('AddCredentialsComponent', () => {
    let component: AddCredentialsComponent;
    let fixture: ComponentFixture<AddCredentialsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddCredentialsComponent],
            imports: [
                RouterTestingModule,
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [EventService],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddCredentialsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
