import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-add-credentials',
    templateUrl: './add-credentials.component.html',
    styleUrls: ['./add-credentials.component.scss'],
})
export class AddCredentialsComponent implements OnInit {
    platforms = [
        {name: 'Spotify', link: ''},
        {name: 'Youtube', link: 'youtube'},
    ];
    selected = this.platforms[0];

    constructor() {
    }

    ngOnInit() {
        this.createOAuthSpotifyLink();
    }

    createOAuthSpotifyLink(): string {
        const clientId = 'f4a594d3c768443eb13af4a222bda19b';
        const uri = encodeURIComponent(window.location.protocol + '//' + window.location.host + '/account/addcredentials/spotify');
        let link = 'https://accounts.spotify.com/authorize?client_id=';
        link += clientId;
        link += '&response_type=code';
        link += '&redirect_uri=';
        link += uri;
        link += '&scope=user-read-private%20user-read-email%20streaming&state=34fFs29kd09';

        return link;
    }

}
