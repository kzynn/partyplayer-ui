import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AccountService, Configuration} from 'partyplayer-api';
import {environment} from '../../../../environments/environment';
import {CurrentUserService} from '../../../services/account/current-user.service';
import {AccountComponent} from './account.component';
import {MatDialogModule} from '@angular/material';

describe('AccountComponent', () => {
    let component: AccountComponent;
    let fixture: ComponentFixture<AccountComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AccountComponent],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                MatDialogModule,
            ],
            providers: [
                AccountService,
                {
                    provide: Configuration,
                    useFactory: (currentUserService: CurrentUserService) => new Configuration({
                        basePath: environment.apiUrl,
                        apiKeys: {Authorization: 'Bearer ' + currentUserService.getUserToken()},
                    }),
                    deps: [CurrentUserService],
                    multi: false,
                },
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
