import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AccountService, SpotifyService} from 'partyplayer-api';
import {CurrentUserService} from '../../../services/account/current-user.service';
import {DeleteEventDialog} from '../../event/event-page/event-page.component';
import {ErrorStateMatcher, MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
    public accountData$;

    constructor(private currentUserService: CurrentUserService, private router: Router,
                private accountService: AccountService, private dialog: MatDialog) {
        this.accountData$ = this.accountService.accountData();
    }

    ngOnInit() {
    }

    refreshData() {
        this.accountData$ = this.accountService.accountData();
    }

    public logout() {
        this.currentUserService.logout();
        this.router.navigateByUrl('/');
    }

    openDeleteDialog(type: string) {
        const dialogRef = this.dialog.open(DeleteTokenDialog, {
            id: 'deleteEventDialog',
            height: 'auto',
            width: 'auto',
            data: type,
        });

        dialogRef.afterClosed().subscribe(result => {
            this.refreshData();
        });
    }

    openUpdateUsernameDialog() {
        this.dialog.open(UpdateUsernameDialog, {
            id: 'UpdateUsernameDialog',
            height: 'auto',
            width: 'auto',
        });
    }

    delete() {
        this.accountService.deleteAccount().subscribe(
            val => {
                this.currentUserService.logout();
                this.router.navigateByUrl('/');
            },
            error => {

            },
            () => {

            }
        );
    }
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'account-delete-token-dialog',
    templateUrl: 'account-delete-token-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class DeleteTokenDialog implements OnInit {
    constructor(public dialogRef: MatDialogRef<DeleteEventDialog>,
                private spotifyService: SpotifyService,
                @Inject(MAT_DIALOG_DATA) public data: string,
    ) {
    }

    ngOnInit(): void {
    }

    removeToken() {
        if (this.data === 'Spotify') {
            this.spotifyService.deleteAccessToken().subscribe();
        }
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'account-update-username-dialog',
    templateUrl: 'account-update-username-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class UpdateUsernameDialog implements OnInit {
    public username = '';
    usernameFormControl = new FormControl('', [
        Validators.required,
    ]);
    matcher = new MyErrorStateMatcher();

    constructor(public dialogRef: MatDialogRef<UpdateUsernameDialog>,
                private accountService: AccountService,
    ) {
    }

    ngOnInit(): void {
    }

    updateUsername() {
        this.accountService.updateUsername({username: this.username})
            .subscribe(res => this.dialogRef.close());
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

