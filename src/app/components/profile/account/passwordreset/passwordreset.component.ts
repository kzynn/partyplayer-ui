import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AccountService} from 'partyplayer-api';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {passwordVerifyCheck} from '../../register/register.component';

@Component({
    selector: 'app-passwordreset',
    templateUrl: './passwordreset.component.html',
    styleUrls: ['./passwordreset.component.scss'],
})
export class PasswordresetComponent implements OnInit {

    hide = true;

    eventUpdate = new FormGroup({
        passwords: new FormGroup({
            password: new FormControl('', [Validators.required]),
            passwordVerify: new FormControl('', [Validators.required]),
        }, passwordVerifyCheck()),
    });

    eventInit = new FormGroup({
        email: new FormControl('', [Validators.email, Validators.required]),
    });

    public isInit = true;
    public doneUpdate = false;
    public doneInit = false;
    public error = '';
    private email: string;
    private token: string;

    constructor(private route: ActivatedRoute, private accountService: AccountService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.email = params.email;
            this.token = params.token;
        });

        if (this.email !== undefined && this.token !== undefined) {
            this.isInit = false;
        }
    }

    init() {
        if (!this.eventInit.valid) {

        }

        this.accountService
            .passwordresetInit(this.eventInit.get('email').value)
            .subscribe(
                () => {
                    this.doneInit = true;
                },
                () => {
                    this.error = 'Sorry, da ist wohl was schief gelaufen';
                });
    }

    public setPassword() {
        this.accountService
            .passwordreset(this.email, this.token, {password: this.eventUpdate.get('passwords').get('password').value})
            .subscribe(
                () => {
                    this.doneUpdate = true;
                },
                () => {
                    this.error = 'Sorry, da ist wohl was schief gelaufen. ' +
                        'Ist der Link korrekt? Vielleicht wurde er bereits verwendet. ' +
                        'Erstelle dir einfach einen neuen!';
                });
    }

    getEmailErrorMessage(): string {
        if (this.getEmail().hasError('required')) {
            return 'Please specify a email for your account.';
        }

        if (this.getEmail().hasError('email')) {
            return 'Your email has no valid format.';
        }

        if (this.getEmail().hasError('inUse')) {
            return 'This email is already taken';
        }
    }

    getPasswordErrorMessage(): string {
        if (this.getPassword().hasError('required')) {
            return 'Please specify a password.';
        }
    }

    getPasswordVerifyErrorMessage(): string {
        if (this.getPasswordVerify().hasError('required')) {
            return 'Please validate your password.';
        }

        if (this.getPasswordGroup().hasError('mismatch')) {
            return 'Your passwords do not match.';
        }
    }

    private getEmail() {
        return this.eventInit.get('email');
    }

    private getPasswordGroup() {
        return this.eventUpdate.get('passwords');
    }

    private getPassword() {
        return this.eventUpdate.get('passwords').get('password');
    }

    private getPasswordVerify() {
        return this.eventUpdate.get('passwords').get('passwordVerify');
    }
}
