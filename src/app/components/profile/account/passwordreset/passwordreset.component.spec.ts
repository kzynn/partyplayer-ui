import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PasswordresetComponent} from './passwordreset.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {AccountService, Configuration} from 'partyplayer-api';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('PasswordresetComponent', () => {
    let component: PasswordresetComponent;
    let fixture: ComponentFixture<PasswordresetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule,
            ],
            declarations: [PasswordresetComponent],
            providers: [AccountService, Configuration],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PasswordresetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
