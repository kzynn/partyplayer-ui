import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService, Configuration, JwtResponse} from 'partyplayer-api';
import {Observable} from 'rxjs';
import {CurrentUserService} from '../../../services/account/current-user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    hide = true;

    event = new FormGroup({
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required]),
    });

    public errorMessage = '';

    constructor(private router: Router, private accountService: AccountService,
                private currentUserService: CurrentUserService, private configuration: Configuration,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    login(): void {
        if (this.event.valid) {

            const userCredentials = {
                username: this.getUsername().value,
                password: this.getPassword().value,
            };

            let response: Observable<JwtResponse>;
            response = this.accountService.accountLogin(userCredentials);

            let tokenString: string;

            response.subscribe(
                res => {
                    tokenString = res.token;

                    this.configuration.apiKeys = {Authorization: 'Bearer ' + tokenString};
                    this.currentUserService.setUser({username: 'username', token: tokenString});


                    this.router.navigate(['event']);
                }, () => {
                    this.errorMessage = 'Benutzername oder Passwort falsch';
                });
        }
    }

    getUsernameErrorMessage(): string {
        return this.getUsername().hasError('required')
            ? 'Please enter a username for your account'
            : 'Some error occured.';
    }

    getPasswordErrorMessage(): string {
        if (this.getPassword().hasError('required')) {
            return 'Please specify a password.';
        }
    }

    private getUsername() {
        return this.event.get('username');
    }

    private getPassword() {
        return this.event.get('password');
    }

}
