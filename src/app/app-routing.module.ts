import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {environment} from './../environments/environment';
import {AddSongComponent} from './components/event/add-song/add-song.component';
import {CreateEventPageComponent} from './components/event/create-event-page/create-event-page.component';
import {EventListComponent} from './components/event/event-list/event-list.component';
import {EventPageComponent} from './components/event/event-page/event-page.component';
import {InvitationComponent} from './components/event/invitation/invitation.component';
import {LandingPageComponent} from './components/landing-page/landing-page.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {AccountComponent} from './components/profile/account/account.component';
import {AddCredentialsComponent} from './components/profile/account/add-credentials/add-credentials.component';
// tslint:disable-next-line:max-line-length
import {AddSpotifyCredentialsComponent} from './components/profile/account/add-credentials/add-spotify-credentials/add-spotify-credentials.component';
import {LoginComponent} from './components/profile/login/login.component';
import {RegisterComponent} from './components/profile/register/register.component';
import {IsLoggedInGuard} from './guards/is-logged-in/is-logged-in.guard';
import {ActivateComponent} from './components/profile/activate/activate.component';
import {PasswordresetComponent} from './components/profile/account/passwordreset/passwordreset.component';

const guards: any[] = environment.mock ? [] : [IsLoggedInGuard];

const routes: Routes = [
    {path: '', pathMatch: 'full', component: LandingPageComponent},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {
        path: 'event',
        canActivate: guards,
        children: [
            {path: '', component: EventListComponent},
            {path: 'create', component: CreateEventPageComponent},
            {path: ':eventId/addsong', component: AddSongComponent},
            {path: ':eventId', component: EventPageComponent},
            {path: ':eventId/invitation/:link', component: InvitationComponent},
        ],
    },
    {
        path: 'account',
        canActivate: guards,
        children: [
            {path: '', component: AccountComponent},
            {path: 'activate/:email/:token', component: ActivateComponent},
            {
                path: 'addcredentials',
                children: [
                    {path: '', component: AddCredentialsComponent},
                    {path: 'spotify', component: AddSpotifyCredentialsComponent},
                ],
            },
        ],
    },
    {
        path: 'account/passwordreset',
        children: [
            {path: '', component: PasswordresetComponent},
            {path: ':email/:token', component: PasswordresetComponent},
        ],
    },
    {path: '**', component: NotFoundComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule {
}
