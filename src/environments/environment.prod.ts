export const environment = {
    production: true,
    mock: false,
    apiUrl: 'https://api.partyplayer.xyz',
};
