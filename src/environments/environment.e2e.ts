export const environment = {
    production: true,
    mock: true,
    apiUrl: 'https://api.partyplayer.xyz',
};
