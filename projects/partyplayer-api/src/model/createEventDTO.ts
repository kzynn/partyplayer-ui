/**
 * Swagger PartyPlayer API
 * API for PartyPlayer Project. Part of Software Engineering at DHBW-Karlsruhe
 *
 * OpenAPI spec version: 1.0.0
 * Contact: info@sebamomann.de
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface CreateEventDTO { 
    /**
     * Name of the created event
     */
    name?: string;
    /**
     * Description of the created event
     */
    description?: string;
    /**
     * Date on which the event will take place
     */
    date?: string;
}
