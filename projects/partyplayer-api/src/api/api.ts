export * from './account.service';
import { AccountService } from './account.service';
export * from './event.service';
import { EventService } from './event.service';
export * from './song.service';
import { SongService } from './song.service';
export * from './spotify.service';
import { SpotifyService } from './spotify.service';
export const APIS = [AccountService, EventService, SongService, SpotifyService];
