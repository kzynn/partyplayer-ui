FROM node:12 AS builder
WORKDIR /app

COPY . /app
RUN npm ci
RUN npm i -g @angular/cli
RUN ng build partyplayer-api
RUN ng build --prod

FROM nginx:alpine
COPY --from=builder /app/dist/* /usr/share/nginx/html/
COPY --from=builder /app/nginx-default.conf /etc/nginx/conf.d/default.conf
